#include <cmath>
#include <iostream>
#include <vector>
#include <iomanip>

using namespace std;

double fact(int n)
{
  double ans = 1;
  for(int i = 1; i <= n; i++)
  {
	ans *= i;
  }
  return ans;
}

double my_cos(double rad)
{
  const int size = 11;
  vector<double> coefs;
  for(int i = 0; i < size; i++)
  {
	coefs.push_back(pow(-1.0, i + 1) / fact((i + 1) * 2));
  }
  double ans = 0.0;
  for(int i = size - 1; i >= 0; i--)
  {
	ans += coefs[i];
	ans *= rad * rad;
  }
  ans += 1.0;
  return ans;
}

int main()
{
  vector<double> rads;
  const int size = 18000;
  for(int i = 0; i < size; i++)
  {
	rads.push_back(M_PI * i / size);
  }

  double max_diff = -1e100;
  for(auto rad : rads)
  {
	max_diff = max(max_diff, abs(my_cos(rad) - cos(rad)));
  }
  cout << scientific << setprecision(20);
  cout << max_diff << endl;
}
