#pragma once
#include <cstdint>

#include "DataHolder.h"

class Solution
{
public:
  Solution(const uint32_t max_id);
  Solution(const std::vector<uint32_t> &vs, const uint32_t max_id);
  ~Solution();
  
  void flip(uint32_t from, uint32_t to);
  uint32_t &next(uint32_t id) const;
  uint32_t &prev(uint32_t id) const;
  uint32_t operator[](uint32_t index) const;
  size_t size() const;
  uint32_t get_max_id() const;

  void copy_from(const Solution &src);
  
private:
  uint32_t index_of(const uint32_t id) const;
 
  uint32_t *array;
  // 逆引き
  uint32_t *indices;

  uint32_t _size;
  const uint32_t max_id;
};

uint32_t Solution::get_max_id() const
{
  return max_id;
}

uint32_t Solution::operator[](uint32_t index) const
{
  return array[index];
}

size_t Solution::size() const
{
  return _size;
}

void Solution::copy_from(const Solution &src)
{
  for(int i = 0; i < max_id; i++)
  {
	array[i] = src.array[i];
	indices[i] = src.indices[i];
  }
}

Solution::Solution(const uint32_t max_id)
  : max_id(max_id)
  , _size(max_id)
{
  array = new uint32_t[max_id];
  indices = new uint32_t[max_id];
  for(int i = 0; i < max_id; i++)
  {
	array[i] = i;
	indices[i] = i;
  }
}

Solution::Solution(const std::vector<uint32_t> &vs, const uint32_t max_id)
  : max_id(max_id)
  , _size(vs.size())
{
  array = new uint32_t[max_id];
  indices = new uint32_t[max_id];

  for(int i = 0; i < vs.size(); i++)
  {
	array[i] = vs[i];
	indices[vs[i]] = i;
  }
}
  
Solution::~Solution()
{
  delete[] array;
  delete[] indices;
}


uint32_t &Solution::next(uint32_t id) const
{
  uint32_t index = (indices[id] == size() - 1 ? 0 : indices[id] + 1);
  return array[index];
}


uint32_t &Solution::prev(uint32_t id) const
{
  uint32_t index = (indices[id] == 0 ? size() - 1 : indices[id] - 1);
  return array[index];
}


uint32_t Solution::index_of(uint32_t id) const
{
  return indices[id];
}

//[from, to]

void Solution::flip(uint32_t id1, uint32_t id2)
{
  uint32_t index1 = indices[id1];
  uint32_t index2 = indices[id2];

  // if((index1 < index2 && index2 - index1 + 1 > size() / 2) ||
  // 	 (index1 > index2 && s + index2 - index1 + 1 > size() / 2))
  // {
  // 	flip(next(id2), prev(id1));
  // }
  
  while(index1 != index2)
  {
	std::swap(indices[array[index1]], indices[array[index2]]);
	std::swap(array[index1], array[index2]);
	index1 = (index1 == size() - 1 ? 0 : index1 + 1);
	if(index1 != index2) index2 = (index2 == 0 ? size() - 1 : index2 - 1);
  }
}

std::ostream &operator<<(std::ostream &out, const Solution &solution)
{
  uint32_t id = 0;
  out << "[";
  for(int i = 0; i < solution.size(); i++)
  {
	out << id << " ";
	id = solution.next(id);
  }
  out << "]";
  return out;
}
