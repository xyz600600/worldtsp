#include <iostream>
#include <vector>
#include <cmath>
#include <random>
#include <cstdint>
#include <cassert>
#include <chrono>
#include <queue>
#include <unordered_set>
#include <set>
#include <iomanip>
#include <map>
#include <algorithm>
#include <unordered_map>
#include <fstream>

#include "DataHolder.h"
#include "KDTree.h"
#include "Solution.h"
#include "TwoLevelTreeV2.h"
#include "UnionFind.h"
#include "Kruskal.h"
#include "test.h"
#include "SubGraphGenerator.h"

using namespace std;

template<typename SOLUTION>
void save_tour(const SOLUTION &sol, const string filename)
{
  ofstream out(filename);
  
  uint32_t id = 0;
  while(!sol.contain(id))
  {
	id++;
  }
  
  const uint32_t end_id = id;
  do
  {
	out << id;
	id = sol.next(id);
	if(id != end_id) out << " ";
  }
  while(id != end_id);
  out << endl;
  
  out.close();
}

void save_graph(const vector<vector<uint32_t>> &graph, const string filename)
{
  ofstream out(filename);

  for(auto &vs : graph)
  {
	for(auto &v : vs)
	{
	  out << v << " ";
	}
	out << endl;
  }
}

template<typename SOLUTION>
uint64_t calculate_tour_length(const SOLUTION &sol, const DataHolder &data)
{
  uint64_t ans = 0;
  
  uint32_t id = 0;
  while(!sol.contain(id))
  {
	id++;
  }
  
  const uint32_t end_id = id;
  do
  {
	ans += data.distance_of(id, sol.next(id));
	id = sol.next(id);
  }
  while(id != end_id);

  return ans;
}

void collect_new_node(set<uint32_t> &new_nodes, const uint32_t new_segment_size,
					  const KDTree &tree_all, const TwoLevelTree &sol,
					  const uint32_t initial_id, const uint32_t all_segment_size)
{
  // insert new solution
  
  priority_queue<pair<uint64_t, uint32_t>> que_new_nodes;
  // RadixHeap que_new_nodes;
  tree_all.nearest_search(initial_id, all_segment_size, que_new_nodes);
  
  // 新しいノードだけを抽出して選択
  while(!que_new_nodes.empty() and new_nodes.size() < new_segment_size)
  {
	if(!sol.contain(que_new_nodes.top().second))
	{
	  new_nodes.insert(que_new_nodes.top().second);
	}
	que_new_nodes.pop();
  }
  assert(new_segment_size == new_nodes.size());

  for(auto v : new_nodes)
  {
	assert(!sol.contain(v));
  }  
}

void dfs(const unordered_map<uint32_t, uint32_t> &id_conv, const vector<vector<uint32_t>> &graph, 
		 vector<bool> &visited, const uint32_t id, TwoLevelTree &sol)
{
  visited[id_conv.at(id)] = true;
  
  for(auto next_id : graph[id_conv.at(id)])
  {
	if(!visited[id_conv.at(next_id)])
	{
	  sol.insert(next_id, id);
	  dfs(id_conv, graph, visited, next_id, sol);
	}
  }
}

// new_nodesは全て消去される
// solには全てのnew_nodes内の都市が挿入される
void insert_new_nodes(set<uint32_t> &new_nodes, TwoLevelTree &sol, const KDTree &tree_sub)
{
  const uint32_t new_segment_size = new_nodes.size();
  // ノード集合を加えたKDTreeに対して，元のノードが少なくとも1つは含まれるように点を選択
  unordered_map<uint32_t, vector<pair<uint64_t, uint32_t>>> vec_map;
  for(auto id : new_nodes)
  {
	std::priority_queue<std::pair<uint64_t, uint32_t>> que;
	// RadixHeap que;
	tree_sub.nearest_search(id, new_segment_size + 1, que);

	vec_map[id].resize(que.size());
	int i = que.size() - 1;
	while(!que.empty())
	{
	  vec_map[id][i--] = que.top();
	  que.pop();
	}
  }

  vector<pair<uint64_t, pair<uint32_t, uint32_t>>> vs;
  for(auto id : new_nodes)
  {
  	for(auto dist_id : vec_map[id])
  	{
	  vs.push_back(make_pair(dist_id.first, make_pair(id, dist_id.second)));
  	}
  }
  sort(vs.begin(), vs.end());

  unordered_map<uint32_t, uint32_t> id_conv;
  uint32_t index = 0;
  for(auto v : new_nodes)
  {
	id_conv[v] = index++;
  }
  
  vector<vector<uint32_t>> graph(new_segment_size);
  UnionFind uf(new_segment_size + 1);
  uint32_t edge_counter = 0;

  set<pair<uint32_t, uint32_t>> orig_set;

  int vs_index = 0;
  while(vs_index < vs.size() and edge_counter < new_segment_size)
  {
  	uint32_t id_orig;
  	uint32_t id_new;
  	uint64_t dist;
  	auto p = vs[vs_index];
  	tie(id_new, id_orig) = p.second;
  	dist = p.first;

	uint32_t conv_id_orig = sol.contain(id_orig) ? new_segment_size : id_conv[id_orig];
	uint32_t conv_id_new  = id_conv[id_new];

	if(uf.find(conv_id_new) != uf.find(conv_id_orig))
	{
	  uf.uni(conv_id_new, conv_id_orig);
	  if(conv_id_orig < new_segment_size)
	  {
		graph[conv_id_new].push_back(id_orig);
		graph[conv_id_orig].push_back(id_new);
	  }
	  else
	  {
		orig_set.insert(make_pair(id_orig, id_new));
	  }
	}
	vs_index++;
  }
  
  vector<bool> visited(new_segment_size);
  for(auto orig_new : orig_set)
  {
	sol.insert(orig_new.second, orig_new.first);
	dfs(id_conv, graph, visited, orig_new.second, sol);
  }  
}

template<typename SOLUTION>
void opt2(const KDTree &tree_sub, const uint32_t neighbor_size,
		  SOLUTION &sol, unordered_set<uint32_t> &dont_look_bit, unordered_set<uint32_t> &improve_node)
{
  const DataHolder &data = tree_sub.get_data();
  
  // 近傍テーブルを作成する
  unordered_map<uint32_t, vector<uint32_t>> neighbor_table;
  
  for(auto v : dont_look_bit)
  {
	std::priority_queue<std::pair<uint64_t, uint32_t>> que;
	// RadixHeap que;
	tree_sub.nearest_search(v, neighbor_size, que);

	for(int j = 0; j < neighbor_size; j++)
	{
	  neighbor_table[v].push_back(que.top().second);
	  que.pop();
	}
  }

  uint64_t tour_length = calculate_tour_length(sol, data);
  const uint64_t init_tour_length = tour_length;
  
  // 2-opt
  auto start = chrono::system_clock::now();
  
  while(!dont_look_bit.empty())
  {
	unordered_set<uint32_t> rem_set;
	
	for(auto id1 : dont_look_bit)
	{
	  bool success = false;

	  if(neighbor_table[id1].empty())
	  {
		std::priority_queue<std::pair<uint64_t, uint32_t>> que;
		// RadixHeap que;
		tree_sub.nearest_search(id1, neighbor_size, que);
		
		for(int j = 0; j < neighbor_size; j++)
		{
		  neighbor_table[id1].push_back(que.top().second);
		  que.pop();
		}
	  }
	  for(auto id3 : neighbor_table[id1])
	  {
		for(int i = 0; i < 2; i++)
		{
		  const uint32_t id2 = (i == 0 ? sol.next(id1) : sol.prev(id1));
		  const uint32_t id4 = (i == 0 ? sol.next(id3) : sol.prev(id3));
		  
		  if(id1 != id4 && id2 != id3)
		  {
			uint32_t id1s[4] = {id1, id3, id1, id2};
			uint32_t id2s[4] = {id2, id4, id3, id4};
			uint64_t dists[4];
			data.distance_of_4(id1s, id2s, dists);
			
			const int64_t gain = dists[0] + dists[1] - dists[2] - dists[3];
			if(gain > 0)
			{
			  if(i == 0)
			  {
				sol.flip(id2, id3);
			  }
			  else
			  {
				sol.flip(id3, id2);
			  }
			  success = true;
			  tour_length -= gain;

			  dont_look_bit.insert(id2);
			  dont_look_bit.insert(id3);
			  dont_look_bit.insert(id4);

			  improve_node.insert(id1);
			  improve_node.insert(id2);
			  improve_node.insert(id3);
			  improve_node.insert(id4);
			  goto NEXT;
			}
		  }
		}
	  }
	NEXT:
	  if(!success)
	  {
		rem_set.insert(id1);
	  }
	}
	for(auto id : rem_set)
	{
	  dont_look_bit.erase(id);
	}
  }
  auto diff = chrono::system_clock::now() - start;
  cerr << "time for 1 loop: " << std::chrono::duration_cast<std::chrono::milliseconds>(diff).count() << endl;
  cerr << "# city = " << tree_sub.size() << endl;
  cerr << "tour: " << init_tour_length << " -> " << tour_length << endl;
}

void select_order(const DataHolder &data,
				  const vector<vector<uint32_t>> &graph,
				  vector<pair<uint32_t, uint32_t>> &order,
				  uint32_t init_id)
{
  vector<bool> visited(graph.size());
  fill(visited.begin(), visited.end(), false);

  using pq_type = pair<uint64_t, pair<uint32_t, uint32_t>>;
  priority_queue<pq_type, vector<pq_type>, greater<pq_type>> que;
  que.push(make_pair(0, make_pair(init_id, init_id)));
  
  while(!que.empty())
  {
	auto dist_from_to = que.top();
	que.pop();
	
	const uint32_t id = dist_from_to.second.second;
	
	order.push_back(dist_from_to.second);
	visited[id] = true;
		
	for(auto next : graph[id])
	{
	  if(!visited[next])
	  {
		que.push(make_pair(data.distance_of(id, next), make_pair(id, next)));
	  }
	}
  }
}

void solve2(uint32_t initial_id)
{
  // TODO: コンパイル時に色々できるようになりたい
  const size_t size = 1904711;
  // const size_t size = 10000;
  const DataHolder data("/home/xyz600/data/world.tsp", size);
  
  KDTree tree_all(data);
  SubGraphGenerator generator(data, 100);
  // Kruskal kruskal(tree_all);
  
  vector<vector<uint32_t>> graph(size);
  generator.getMST(graph);

  // save_graph(graph, "/ramdisk/tour/data/graph.txt");
  
  vector<std::pair<uint32_t, uint32_t>> order;
  select_order(data, graph, order, initial_id);

  const size_t no_segment = sqrt(size);
  const uint32_t neighbor_size = 200;

  // KD-tree初期化用
  vector<uint32_t> current_cities = {initial_id};

  TwoLevelTree sol(current_cities, size);

  unordered_set<uint32_t> dont_look_bit;
  unordered_set<uint32_t> improve_node;

  uint32_t prev_size = 1;
  
  for(int i = 1; i <= no_segment; i++)
  {
	auto start = chrono::system_clock::now();
	
	const uint32_t new_size = (size * i) / no_segment;
	const uint32_t new_segment_size = new_size - prev_size;
	
	// 新しいノードだけを抽出して選択
	set<uint32_t> new_nodes;
	for(uint32_t v = prev_size; v < new_size; v++)
	{
	  new_nodes.insert(order[v].second);
	  current_cities.push_back(order[v].second);
	}
	
	KDTree tree_sub(data, current_cities);
	
	if(i == 1)
	{
	  shuffle(current_cities.begin(), current_cities.end(), mt19937());
	  Solution s(current_cities, size);
	  sol.init(s);
	}
	else
	{
	  for(uint32_t j = prev_size; j < new_size; j++)
	  {
		uint32_t from, to;
		std::tie(from, to) = order[j];
		sol.insert(to, from);
	  }
	  sol.rebuild();
	}

	for(auto v : new_nodes)
	{
	  dont_look_bit.insert(v);
	}
	opt2(tree_sub, neighbor_size, sol, dont_look_bit, improve_node);
	auto diff = chrono::system_clock::now() - start;
	cerr << "time for 1 segment opt: " << std::chrono::duration_cast<std::chrono::milliseconds>(diff).count() << endl;

	std::swap(dont_look_bit, improve_node);
	
	prev_size = new_size;
  }
  save_tour(sol, "tour.txt");
}

void solve1()
{
  const size_t size = 1904711;
  const DataHolder data("/home/xyz600/data/world.tsp", size); 

  // 近傍テーブルを作成する
  KDTree tree(data);
  const uint32_t neighbor_size = 200;
  
  vector<array<uint32_t, neighbor_size>> neighbor_table(size);
  for(size_t i = 0; i < size; i++)
  {
	if(i % 100000 == 0) cerr << i << endl;
	std::priority_queue<std::pair<uint64_t, uint32_t>> que;

	tree.nearest_search(i, neighbor_size, que);

	for(int j = 0; j < neighbor_size; j++)
	{
	  neighbor_table[i][j] = que.top().second;
	  que.pop();
	}
  }

  Solution s(size);
  TwoLevelTree sol(s);
  int counter = 0;
  uint64_t tour_length = calculate_tour_length(sol, data);
  
  for(int i = 0; i < 100; i++)
  {
	// 2-opt
	unordered_set<uint32_t> dont_look_bit;
	for(int i = 0; i < size; i++)
	{
	  dont_look_bit.insert(i);
	}
	
	while(!dont_look_bit.empty())
	{
	  cerr << "tour: " << tour_length << endl;
	  auto start = chrono::system_clock::now();
	  
	  set<uint32_t> rem_set;
	  
	  for(auto id1 : dont_look_bit)
	  {
		bool success = false;
		for(auto id3 : neighbor_table[id1])
		{
		  const uint32_t id2 = sol.next(id1);
		  const uint32_t id4 = sol.next(id3);

		  if(id1 != id4 && id2 != id3)
		  {
			uint32_t id1s[4] = {id1, id3, id1, id2};
 			uint32_t id2s[4] = {id2, id4, id3, id4};
			uint64_t dists[4];
			data.distance_of_4(id1s, id2s, dists);
			const int64_t gain = dists[0] + dists[1] - dists[2] - dists[3];
			if(gain > 0)
			{
			  sol.flip(id2, id3);
			  success = true;
			  tour_length -= gain;
			  if(++counter % 100000 == 0) cout << "tour: " << tour_length << endl;

			  dont_look_bit.insert(id2);
			  dont_look_bit.insert(id3);
			  dont_look_bit.insert(id4);
			  break;
			}
		  }
		}
		if(!success)
		{
		  rem_set.insert(id1);
		}
	  }
	  for(auto id : rem_set)
	  {
		dont_look_bit.erase(id);
	  }
	  auto diff = chrono::system_clock::now() - start;
	  cout << "time for 1 loop: " << std::chrono::duration_cast<std::chrono::milliseconds>(diff).count() << endl;
	}
  }  
}

int main(int argc, char *argv[])
{
  // test(initial_id);
  // return 0;

  const uint32_t initial_id = stoi(argv[1]);
  cerr << "initial id = " << initial_id << endl;
  solve2(initial_id);
  // solve1();
}
