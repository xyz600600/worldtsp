from pylab import *

xs = []
ys = []

home = '/ramdisk/tour/'

for line in open('/home/xyz600/data/world.tsp').readlines():
    tokens = line.strip().split(" ")
    ys.append(float(tokens[1]))
    xs.append(float(tokens[2]))

clf()
xlim([-1800000, 1800000])
ylim([-800000, 800000])

for i, line in enumerate(open(home + "data/graph.txt").readlines()):
    nodes = [int(item) for item in line.strip().split(" ")]
    for j in nodes:
        if i < j:
            plot([xs[i], xs[j]], [ys[i], ys[j]], 'b-', linewidth=0.15)
    if i % 100 == 0:
        print(i)
    if i > 0 and i % 300000 == 0:
        break

savefig(home + 'png_img/graph.png', dpi=500)
