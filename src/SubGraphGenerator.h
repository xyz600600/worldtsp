#pragma once

#include <algorithm>
#include <vector>
#include <cstdint>
#include <iostream>

#include "DataHolder.h"
#include "UnionFind.h"

class SubGraphGenerator
{
public:
  SubGraphGenerator(const DataHolder &data, const int div);

  void getMST(std::vector<std::vector<uint32_t>> &graph);
  
private:
  const DataHolder &data;
  
  const int div;

  void gen_subgraph(std::vector<std::vector<uint32_t>> &graph);

  bool cross(const double sr1, const double er1, const double sr2, const double er2);

  int next(const int i);

  int prev(const int i);

  void connect(const std::pair<int, int> &r1, const std::pair<int, int> &r2,
			   std::vector<std::vector<uint32_t>> &graph,
			   const std::vector<uint32_t> &ids);
};

// ---- 実装 ----

bool SubGraphGenerator::cross(const double sr1, const double er1, const double sr2, const double er2)
{
  return sr1 <= er2 && sr2 <= er1;
}   

int SubGraphGenerator::next(const int i)
{
  return i == div - 1 ? 0 : i + 1;
}

int SubGraphGenerator::prev(const int i)
{
  return i == 0 ? div - 1 : i - 1;
}

// r2の点をr1に登録する
void SubGraphGenerator::connect(const std::pair<int, int> &r1, const std::pair<int, int> &r2,
								std::vector<std::vector<uint32_t>> &graph,
								const std::vector<uint32_t> &ids)
{
  for(int i = r1.first; i < r1.second; i++)
  {
	for(int j = r2.first; j < r2.second; j++)
	{
	  if(i != j)
	  {
		graph[ids[i]].push_back(ids[j]);
	  }
	}
  }
}

SubGraphGenerator::SubGraphGenerator(const DataHolder &data, const int div)
  : data(data)
  , div(div){}

static void i_save_graph(const std::vector<std::vector<uint32_t>> &graph, const std::string filename)
{
  std::ofstream out(filename);

  for(auto &vs : graph)
  {
	for(auto &v : vs)
	{
	  out << v << " ";
	}
	out << std::endl;
  }
}

void SubGraphGenerator::gen_subgraph(std::vector<std::vector<uint32_t>> &graph)
{
  std::vector<uint32_t> ids;

  for(int i = 0; i < data.size(); i++)
  {
	ids.push_back(i);
  }

  // 全体をx座標でソート
  auto cmp_long = [&](uint32_t i, uint32_t j){
	return data.longtitude(i) < data.longtitude(j);
  };
  sort(ids.begin(), ids.end(), cmp_long);

  auto cmp_lat = [&](uint32_t i, uint32_t j){
	return data.latitude(i) < data.latitude(j);
  };
  
  std::vector<std::vector<std::pair<int, int>>> cells(div);
  // y座標でソート
  for(int i = 0; i < div; i++)
  {
	const int sx = data.size() * i / div;
	const int ex = data.size() * (i + 1) / div;

	sort(ids.begin() + sx, ids.begin() + ex, cmp_lat);
	for(int j = 0; j < div; j++)
	{
	  const int sy = sx + (ex - sx) * j / div;
	  const int ey = sx + (ex - sx) * (j + 1) / div;
	  cells[i].push_back(std::make_pair(sy, ey));
	}
  }

  // vector<vector<int>> r_graph(div * div);
  
  // 隣接関係を限定した部分グラフを作成
  for(int xi = 0; xi < div; xi++)
  {
	std::vector<int> xs = {prev(xi), next(xi)};
	
	for(int yi = 0; yi < div; yi++)
	{
	  connect(cells[xi][yi], cells[xi][yi], graph, ids);
	  if(yi > 0) connect(cells[xi][yi], cells[xi][yi - 1], graph, ids);
	  if(yi < div - 1) connect(cells[xi][yi], cells[xi][yi + 1], graph, ids);
	  
	  for(auto nxi : xs)
	  {
		for(int nyi = 0; nyi < div; nyi++)
		{
		  if(cross(data.latitude(ids[cells[xi][yi].first]), data.latitude(ids[cells[xi][yi].second]), 
				   data.latitude(ids[cells[nxi][nyi].first]), data.latitude(ids[cells[nxi][nyi].second])))
		  {
			connect(cells[xi][yi], cells[nxi][nyi], graph, ids);
		  }
		}
	  }
	}
  }
  // i_save_graph(graph, "/ramdisk/tour/data/sub_graph.txt");
}

void SubGraphGenerator::getMST(std::vector<std::vector<uint32_t>> &graph)
{
  std::vector<std::vector<uint32_t>> sub_graph(data.size());
  
  gen_subgraph(sub_graph);

  long long ans = 0;
  for(auto &v : sub_graph)
  {
	ans += v.size();
  }
  
  // Kruscal
  std::vector<std::pair<uint64_t, std::pair<uint32_t, uint32_t>>> edge(ans / 2);
  int index = 0;

  for(int i = 0; i < sub_graph.size(); i++)
  {
	for(auto &v : sub_graph[i])
	{
	  const uint64_t d = data.distance_of(i, v);
	  if(i < v) edge[index++] = std::make_pair(d, std::make_pair(i, v));
	}
	if(i % 100000 == 0 ) std::cout << i << std::endl;
  }
  std::sort(edge.begin(), edge.end());
  
  UnionFind uf(data.size());

  graph.clear();
  graph.resize(data.size());

  int count = 0;
  
  for(auto &e : edge)
  {
	int from, to;
	std::tie(from, to) = e.second;

	if(uf.find(from) != uf.find(to))
	{
	  uf.uni(from, to);
	  graph[from].push_back(to);
	  graph[to].push_back(from);
	  count++;
	}
	if(index % 100000000 == 0) std::cout << index << ", " << count << std::endl;
	if(count == data.size() - 1) break;
  }
  assert(count == data.size() - 1);
}
