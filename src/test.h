#pragma once

static const bool debug = false;

bool test(std::vector<std::pair<int, int>> &test)
{
  if(debug) std::cout << "test: " << std::endl;
  const size_t sss = 12;
  Solution s(sss);
  TwoLevelTree t(s);
  if(debug) std::cout << "t: " << t << std::endl;
  if(debug) std::cout << "s: " << s << std::endl;

  for(auto p : test)
  {
	s.flip(p.first, p.second);
	if(debug) std::cout << "(" << p.first << ", " << p.second << ")" << std::endl;
	t.flip(p.first, p.second);
	if(debug) std::cout << "t: " << t << std::endl;
	if(debug) std::cout << "s: " << s << std::endl;
  }
  if(debug) std::cout << "---------------------------------------" << std::endl;

  if(debug) std::cout << "NO_____P__ " << t << std::endl;
  if(debug) std::cout << "t: " << t << std::endl;
  if(debug) std::cout << "s: " << s << std::endl;
  
  uint32_t id = 0;
  for(int i = 0; i < sss; i++)
  {
	if(s.next(id) != t.next(id))
	{
	  if(debug) std::cerr << std::endl << "NO: " << s.next(id) << ", " << t.next(id) << std::endl;
	  t.output_inner_state();
	  return false;
	}
	id = s.next(id);
  }
  return true;
}

std::pair<int, int> mp(int a, int b) { return std::make_pair(a, b); }

void make_testcase(std::vector<std::vector<std::pair<int, int>>> &test_all, int size)
{
  std::vector<std::pair<int, int>> s;
  for(int i = 0; i < size; i++)
  {
	for(int j = 0; j < size; j++)
	{
	  if(i != j) s.push_back(mp(i, j));
	}
  }
  
  for(int i = 0; i < s.size(); i++)
  {
	for(int j = 0; j < s.size(); j++)
	{
	  for(int k = 0; k < s.size(); k++)
	  {
		for(int l = 0; l < s.size(); l++)
		{
		  test_all.push_back({s[i], s[j], s[k], s[l]});
		}
	  }
	}
  }
}

void generate_testcase()
{
  std::vector<std::vector<std::pair<int, int>>> test_all;
  make_testcase(test_all, 12);
  
  int counter = 0;
  int miss_counter = 0;
  for(auto &testcase : test_all)
  {
	if(counter++ % 100000 == 0)   std::cout << miss_counter << " / " << counter << std::endl;
	if(!test(testcase))
	{
	  miss_counter++;
	  std::cout << "[";
	  for(auto p : testcase)
	  {
	  	std::cout << "(" << p.first << ", " << p.second << ")";
	  }
	  std::cout << "]" << std::endl;
	}	
  }
  std::cout << miss_counter << " / " << counter << std::endl;
}

void test_kdtree2(uint32_t id)
{
  const size_t size = 40000;
  const DataHolder data("/home/xyz600/data/world.tsp", size); 
  
  // 近傍テーブルを作成する
  KDTree tree(data);

  const size_t neighbor_size = 50;
  
  std::priority_queue<std::pair<uint64_t, uint32_t>> que;
  // RadixHeap que;
  tree.nearest_search(id, neighbor_size, que);
  while(!que.empty())
  {
	std::cerr << que.top().second << ", ";
	que.pop();
  }
  std::cerr << std::endl;

  std::vector<std::pair<uint64_t, uint32_t>> vs;
  for(int j = 0; j < size; j++)
  {
	if(j != id)
	{
	  auto d = data.distance_of(id, j);
	  vs.push_back(std::make_pair(d, j));
	}
  }
  std::sort(vs.begin(), vs.end());

  for(int i = neighbor_size - 1; i >= 0; i--)
  {
	std::cerr << vs[i].second << ", ";
  }
  std::cerr << std::endl;  
}

void test_kdtree()
{
  const size_t size = 10000;
  const DataHolder data("/home/xyz600/data/world.tsp", size); 
  
  // 近傍テーブルを作成する
  KDTree tree(data);

  for(int i = 0; i < size; i++)
  {
	std::priority_queue<std::pair<uint64_t, uint32_t>> que;
	// RadixHeap que;
	tree.nearest_search(i, 1, que);
	auto nearest = que.top();

	uint32_t min_id = -1;
	uint64_t min_dist = 10000000000000ULL;
	for(int j = 0; j < size; j++)
	{
	  if(j != i)
	  {
		auto d = data.distance_of(i, j);
		if(d < min_dist)
		{
		  min_dist = d;
		  min_id = j;
		}
	  }
	}
	if(min_dist != nearest.first)
	{
	  std::cerr << "fault!" << std::endl;
	  std::cerr << i << ", " << min_id << ", " << nearest.second << ", " << min_dist << ", " << nearest.first << std::endl;
	}
  }
}

void test(uint32_t initial_id)
{
  test_kdtree(); //(initial_id);
  return;
  
  const uint32_t sss = 16;
  Solution s(sss);
  TwoLevelTree t(s);

  generate_testcase();

  exit(0);
}
