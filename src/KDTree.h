#pragma once
#include <array>
#include <vector>
#include <cstdint>
#include <algorithm>
#include <tuple>
#include <queue>
#include <x86intrin.h>

#include "DataHolder.h"
#include "RadixHeap.h" 

struct BoundingBox
{
  const static int LEFT = 0;
  const static int RIGHT = 1;
  const static int TOP = 2;
  const static int BOTTOM = 3;
  
  BoundingBox();
  uint64_t distance_upperbound(double latitude, double longtitude, const DataHolder &data) const;
  uint64_t distance_lowerbound(double latitude, double longtitude, const DataHolder &data) const;
  uint64_t max_distance_upperbound(double latitude, double longtitude, const DataHolder &data) const;
  bool inside(double latitude, double longtitude) const;

  double pos[4];
  
  // [from, to)
  uint32_t from;
  uint32_t to;

  void uni(const BoundingBox &bb1, const BoundingBox &bb2)
  {
	pos[LEFT] = std::min(bb1.pos[LEFT], bb2.pos[LEFT]);
	pos[RIGHT] = std::max(bb1.pos[RIGHT], bb2.pos[RIGHT]);
	pos[TOP] = std::max(bb1.pos[TOP], bb2.pos[TOP]);
	pos[BOTTOM] = std::min(bb1.pos[BOTTOM], bb2.pos[BOTTOM]);
  }

  void append(const double latitude, const double longtitude)
  {
	pos[LEFT] = std::min(pos[LEFT], longtitude);
	pos[RIGHT] = std::max(pos[RIGHT], longtitude);
	pos[TOP] = std::max(pos[TOP], latitude);
	pos[BOTTOM] = std::min(pos[BOTTOM], latitude);
  }
};

std::ostream &operator<<(std::ostream &out, const BoundingBox &bb)
{
  out << bb.pos[BoundingBox::LEFT] << ", " << bb.pos[BoundingBox::RIGHT] << " : " <<
	bb.pos[BoundingBox::BOTTOM] << ", " << bb.pos[BoundingBox::TOP];
  return out;
}

BoundingBox::BoundingBox()
{
  pos[LEFT] = pos[BOTTOM] = 1000000000;
  pos[RIGHT] = pos[TOP] = -1000000000;
}

uint64_t BoundingBox::max_distance_upperbound(double latitude, double longtitude, const DataHolder &data) const
{
  double center_lat = (pos[TOP] + pos[BOTTOM]) / 2;
  double center_long = (pos[LEFT] + pos[RIGHT]) / 2;
  
  const uint64_t r = std::max(data.distance_of(center_lat, center_long, pos[TOP], pos[LEFT]),
							  data.distance_of(center_lat, center_long, pos[BOTTOM], pos[LEFT]));
  
  const uint64_t dist = data.distance_of(center_lat, center_long, latitude, longtitude);
  return dist + r;
}

uint64_t BoundingBox::distance_upperbound(double latitude, double longtitude, const DataHolder &data) const
{
  uint64_t dists[4];
  double longs[4] = {pos[LEFT], pos[RIGHT], pos[LEFT], pos[RIGHT]};
  double lats[4] = {pos[TOP], pos[TOP], pos[BOTTOM], pos[BOTTOM]};
  data.distance_of_4(latitude, longtitude, lats, longs, dists);
  return std::min(std::min(dists[0], dists[1]), std::min(dists[2], dists[3]));
}

// BBを覆う円を考えて，それとの距離を算出
uint64_t BoundingBox::distance_lowerbound(double latitude, double longtitude, const DataHolder &data) const
{
  double center_lat = (pos[TOP] + pos[BOTTOM]) / 2;
  double center_long = (pos[LEFT] + pos[RIGHT]) / 2;

  const uint64_t r = std::max(data.distance_of(center_lat, center_long, pos[TOP], pos[LEFT]),
							  data.distance_of(center_lat, center_long, pos[BOTTOM], pos[LEFT]));

  const uint64_t dist = data.distance_of(center_lat, center_long, latitude, longtitude);
  
  return dist < r ? 0 : dist - r;
}

bool BoundingBox::inside(double latitude, double longtitude) const
{
  return (pos[LEFT] <= longtitude) && (longtitude <= pos[RIGHT]) &&
	(pos[BOTTOM] <= latitude) && (latitude <= pos[TOP]);
}

class KDTree
{
public:
  KDTree(const DataHolder &data);
  KDTree(const DataHolder &data, const std::vector<uint32_t> &vs);
  ~KDTree();
  void nearest_search(const uint32_t id, const size_t num,
					  std::priority_queue<std::pair<uint64_t, uint32_t>> &que,
					  // RadixHeap &que,
					  const int bb_index = 1) const;

  void nearest_search_lower(const uint32_t id, const size_t num,
							std::priority_queue<std::pair<uint64_t, uint32_t>> &que,
							// RadixHeap &que,
							const uint32_t lowerbound_cityid,
							uint64_t lowerbound_dist = -1,
							const int bb_index = 1) const;
  
  uint32_t size() const;
  const DataHolder &get_data() const;
  
private:
  const DataHolder &data;
  std::vector<uint32_t> indices;
  std::vector<BoundingBox> bb_tree;
  
  // [left, right)
  void setup(const uint32_t from, const uint32_t to, int depth, uint32_t index);
  uint32_t left(uint32_t idx) const;
  uint32_t right(uint32_t idx) const;
  uint32_t calculate_left_size(const uint32_t parent_size) const;
  
  const int point_per_box = 32;
  const uint32_t max_id;
  
  uint32_t _size;
};

const DataHolder &KDTree::get_data() const
{
  return data;
}

uint32_t KDTree::size() const
{
  return _size;
}

uint32_t KDTree::left(uint32_t idx) const { return idx * 2; }

uint32_t KDTree::right(uint32_t idx) const { return idx * 2 + 1; }

bool cmp(uint32_t id1, uint64_t dist1, uint32_t id2, uint64_t dist2)
{
  if(dist1 != dist2)
  {
	return dist1 < dist2;
  }
  else
  {
	return id1 < id2;
  }
}

// 近傍リストを分割保持するために，lowerbound_cityid以降のリストを作成
void KDTree::nearest_search_lower(const uint32_t id, const size_t num,
								  std::priority_queue<std::pair<uint64_t, uint32_t>> &que,
								  // RadixHeap &que,
								  const uint32_t lowerbound_cityid,
								  uint64_t lowerbound_dist,
								  const int bb_index) const
{
  if(lowerbound_dist == -1)
  {
	lowerbound_dist = data.distance_of(id, lowerbound_cityid);
  }
  
  // 葉ノードは普通に探索
  if(bb_tree[bb_index].to - bb_tree[bb_index].from <= point_per_box) // 葉ノード
  {
	for(int i = bb_tree[bb_index].from; i < bb_tree[bb_index].to; i++)
	{
	  if(indices[i] != id)
	  {
		uint64_t dist = data.distance_of(id, indices[i]);
		if(cmp(lowerbound_cityid, lowerbound_dist, indices[i], dist) and (num > que.size() or dist <= que.top().first))
		{
		  que.push(std::make_pair(dist, indices[i]));
		}
		if(num < que.size())
		{
		  que.pop();
		}
	  }
	}
  }
  else if(bb_tree[left(bb_index)].inside(data.latitude(id), data.longtitude(id))) // 左ノードに含まれている
  {
	if(bb_tree[left(bb_index)].max_distance_upperbound(data.latitude(id), data.longtitude(id), data) >= lowerbound_dist)
	{
	  nearest_search_lower(id, num, que, lowerbound_cityid, lowerbound_dist, left(bb_index));
	}
	
	if(bb_tree[right(bb_index)].max_distance_upperbound(data.latitude(id), data.longtitude(id), data) >= lowerbound_dist and
	   (que.size() < num or
		bb_tree[right(bb_index)].distance_lowerbound(data.latitude(id), data.longtitude(id), data) < que.top().first))
	{
	  nearest_search_lower(id, num, que, lowerbound_cityid, lowerbound_dist, right(bb_index));
	}
  }
  else // 右ノードに含まれている
  {
	if(bb_tree[right(bb_index)].max_distance_upperbound(data.latitude(id), data.longtitude(id), data) >= lowerbound_dist)
	{
	  nearest_search_lower(id, num, que, lowerbound_cityid, lowerbound_dist, right(bb_index));
	}
	
	if(bb_tree[left(bb_index)].max_distance_upperbound(data.latitude(id), data.longtitude(id), data) >= lowerbound_dist and
	   (que.size() < num or
		bb_tree[left(bb_index)].distance_lowerbound(data.latitude(id), data.longtitude(id), data) < que.top().first))
	{
	  nearest_search_lower(id, num, que, lowerbound_cityid, lowerbound_dist, left(bb_index));
	}
  }
}

void KDTree::nearest_search(const uint32_t id, const size_t num,
							std::priority_queue<std::pair<uint64_t, uint32_t>> &que,
							// RadixHeap &que,
							const int bb_index) const
{
  if(bb_tree[bb_index].to - bb_tree[bb_index].from <= point_per_box) // 葉ノード
  {
	for(int i = bb_tree[bb_index].from; i < bb_tree[bb_index].to; i++)
	{
	  if(indices[i] != id)
	  {
		uint64_t dist = data.distance_of(id, indices[i]);
		if(num > que.size() or dist <= que.top().first)
		{
		  que.push(std::make_pair(dist, indices[i]));
		}
		if(num < que.size())
		{
		  que.pop();
		}
	  }
	}
  }
  else if(bb_tree[left(bb_index)].inside(data.latitude(id), data.longtitude(id))) // 左ノードに含まれている
  {
	nearest_search(id, num, que, left(bb_index));
	if(que.size() < num or bb_tree[right(bb_index)].distance_lowerbound(data.latitude(id), data.longtitude(id), data) < que.top().first)
	{
	  nearest_search(id, num, que, right(bb_index));
	}
  }
  else // 右ノードに含まれている
  {
	nearest_search(id, num, que, right(bb_index));
	if(que.size() < num or bb_tree[left(bb_index)].distance_lowerbound(data.latitude(id), data.longtitude(id), data) < que.top().first)
	{
	  nearest_search(id, num, que, left(bb_index));
	}
  }
}

KDTree::KDTree(const DataHolder &_data, const std::vector<uint32_t> &vs)
  : data(_data)
  , max_id(_data.size())
  , _size(vs.size())
{
  indices.resize(size());
  for(int i = 0; i < size(); i++)
  {
	indices[i] = vs[i];
  }
  bb_tree.resize(size()); // メモリ切り詰めるなら書き直し
  setup(0, size(), 0, 1);
}

KDTree::KDTree(const DataHolder &_data)
  : data(_data)
  , max_id(_data.size())
  , _size(_data.size())
{
  indices.resize(size());
  for(int i = 0; i < size(); i++)
  {
	indices[i] = i;
  }
  bb_tree.resize(size()); // メモリ切り詰めるなら書き直し
  setup(0, size(), 0, 1);
}

uint32_t KDTree::calculate_left_size(const uint32_t parent_size) const
{
  uint32_t left_size = 1;
  while(left_size < parent_size)
  {
	left_size <<= 1;
  }
  return left_size >> 1;
}

void KDTree::setup(const uint32_t from, const uint32_t to, int depth, uint32_t index_bb)
{
  if(to > from + point_per_box)
  {
	if(depth % 2 == 0)
	{
	  auto f = [&](uint32_t i, uint32_t j) -> bool { return data.longtitude(i) < data.longtitude(j); };
	  std::sort(indices.begin() + from, indices.begin() + to, f);
	}
	else
	{
	  auto f = [&](uint32_t i, uint32_t j) -> bool { return data.latitude(i) < data.latitude(j); };
	  std::sort(indices.begin() + from, indices.begin() + to, f);
	}
	const uint32_t left_size = calculate_left_size(to - from);
	setup(from, from + left_size, depth + 1, left(index_bb));
	setup(from + left_size, to, depth + 1, right(index_bb));
	bb_tree[index_bb].uni(bb_tree[left(index_bb)], bb_tree[right(index_bb)]);
  }
  else
  {
	for(int i = from; i < to; i++)
	{
	  bb_tree[index_bb].append(data.latitude(indices[i]), data.longtitude(indices[i]));
	}
  }
  bb_tree[index_bb].from = from;
  bb_tree[index_bb].to = to;
}

KDTree::~KDTree(){} 
