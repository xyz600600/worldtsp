#pragma once

class UnionFind
{
public:
  UnionFind(const uint32_t size);
  ~UnionFind();
  void uni(uint32_t id1, uint32_t id2);
  uint32_t find(uint32_t id);
private:
  uint32_t *parent;
  uint32_t *rank;
};

UnionFind::UnionFind(const uint32_t size)
{
  parent = new uint32_t[size];
  rank = new uint32_t[size];

  for(int i = 0; i < size; i++)
  {
	parent[i] = i;
	rank[i] = 0;
  }  
}

UnionFind::~UnionFind()
{
  delete[] parent;
  delete[] rank;
}

uint32_t UnionFind::find(uint32_t id)
{
  if(parent[id] == id)
  {
	return id;
  }
  else
  {
	parent[id] = find(parent[id]);
	return parent[id];
  }
}

void UnionFind::uni(uint32_t id1, uint32_t id2)
{
  uint32_t pa_1 = find(id1);
  uint32_t pa_2 = find(id2);
  if(rank[pa_1] < rank[pa_2])
  {
	parent[pa_1] = pa_2;
  }
  else if(rank[pa_2] < rank[pa_1])
  {
	parent[pa_2] = pa_1;
  }
  else if(pa_1 != pa_2)
  {
	parent[pa_1] = pa_2;
	rank[pa_2]++;
  }
}
