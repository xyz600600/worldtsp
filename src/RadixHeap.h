#pragma once

#include <algorithm>
#include <vector>

int bsr(uint64_t x)
{
  if(x == 0) return -1;
  else return 63 - __builtin_clz(x);
}

class RadixHeap
{
public:
  RadixHeap();
  void push(const std::pair<uint64_t, uint32_t> &x);
  void pop();
  std::pair<uint64_t, uint32_t> top();	
  size_t size() const;
  bool empty() const;
  
private:
  std::vector<std::pair<uint64_t, uint32_t>> vs[65];
  uint64_t last;
  size_t _size;
  const uint64_t INF = (1ULL << 50);
};

bool RadixHeap::empty() const
{
  return _size == 0;
}

RadixHeap::RadixHeap()
{
  last = _size = 0;
}

void RadixHeap::push(const std::pair<uint64_t,uint32_t> &x)
{
  assert(last <= (INF - x.first));
  vs[bsr((INF - x.first) ^ last) + 1].push_back(x);
  _size++;
}

std::pair<uint64_t, uint32_t> RadixHeap::top()
{
  assert(!empty());
  if(vs[0].empty())
  {
	int index = 1;
	while(vs[index].empty())
	{
	  index++;
	}
	uint64_t new_last = INF - max_element(vs[index].begin(), vs[index].end())->first;
	for(auto v : vs[index])
	{
	  vs[bsr(new_last ^ (INF - v.first)) + 1].push_back(v);
	}
	last = new_last;
	vs[index].clear();
  }
  return vs[0].back();
}

void RadixHeap::pop()
{
  assert(!empty());
  if(vs[0].empty())
  {
	int index = 1;
	while(vs[index].empty())
	{
	  index++;
	}
	uint64_t new_last = INF - max_element(vs[index].begin(), vs[index].end())->first;
	for(auto v : vs[index])
	{
	  vs[bsr(new_last ^ (INF - v.first)) + 1].push_back(v);
	}
	last = new_last;
	vs[index].clear();
  }
  _size--;
  vs[0].pop_back();
}

size_t RadixHeap::size() const
{
  return _size;
}
