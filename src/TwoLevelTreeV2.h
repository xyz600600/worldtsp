#pragma once

#include <deque>
#include <vector>
#include <cstdint>
#include <set>
#include <algorithm>
#include <cmath>

static const bool debug_tree = false;

class TwoLevelTree
{
public:
  TwoLevelTree(const Solution &sol);
  TwoLevelTree(const std::vector<uint32_t> &vs, const uint32_t max_id);
  ~TwoLevelTree();
  
  uint32_t &next(const uint32_t id) const;
  uint32_t &prev(const uint32_t id) const;
  void flip(const uint32_t from, const uint32_t to);
  void rebuild();
  size_t size() const;
  void output_inner_state() const;

  void insert(const uint32_t new_id, const uint32_t orig_id);
  bool contain(const uint32_t city_id) const;
  void init(const Solution &sol);

  void validate() const;
  
private:
  void split(const uint32_t city_from, const uint32_t new_segid);
  // 右隣りとmerge．小さいidを残す.
  void merge(const uint32_t seg_id1);
  
  // aとcの間にbがあるか否かを調べる
  bool betweenInSegment(const uint32_t a, const uint32_t b, const uint32_t c) const;
  void rev_seg(const uint32_t seg_from, const uint32_t seg_to);
  
  uint32_t subsize;
  const uint32_t max_id;
  uint32_t _size;
  
  uint32_t &front(const uint32_t segid);
  uint32_t &back(const uint32_t segid);
  
  void revert_rev(const uint32_t segid);

  void flip_insegment(const uint32_t from, const uint32_t to);
  void flip_outsegment(const uint32_t from, const uint32_t to);

  // city-idベースで持つ
  uint32_t *city_next;
  uint32_t *city_prev;
  uint32_t *city_parent;
  
  uint32_t *segment_back;
  uint32_t *segment_front;
  uint32_t *segment_size;
  bool *segment_rev;
  uint32_t *segment_next;
  uint32_t *segment_prev;
};

template<typename T>
void print_array(const T *data, size_t size)
{
  std::cout << "[";
  for(int i = 0; i < size; i++)
  {
	std::cout << data[i] << " ";
  }
  std::cout << "]" << std::endl;
}

void TwoLevelTree::output_inner_state() const
{
  std::cout << "next:     ";
  print_array(city_next, size());
  
  std::cout << "prev:     ";
  print_array(city_prev, size());
  
  std::cout << "parent:   ";
  print_array(city_parent, size());

  std::cout << "seg_back: ";
  print_array(segment_back, subsize + 2);
  
  std::cout << "seg_fr :  ";
  print_array(segment_front, subsize + 2);

  std::cout << "seg_size: ";
  print_array(segment_size, subsize + 2);

  std::cout << "seg_rev:  ";
  print_array(segment_rev, subsize + 2);

  std::cout << "seg_next: ";
  print_array(segment_next, subsize + 2);

  std::cout << "seg_prev: ";
  print_array(segment_prev, subsize + 2);
}

TwoLevelTree::TwoLevelTree(const std::vector<uint32_t> &vs, const uint32_t max_id)
  : max_id(max_id)
{
  city_next = new uint32_t[max_id];
  city_prev = new uint32_t[max_id];
  city_parent = new uint32_t[max_id];
  
  const uint32_t max_subsize = ceil(sqrt(max_id));
  
  segment_back = new uint32_t[max_subsize * 2];
  segment_front = new uint32_t[max_subsize * 2];
  segment_size = new uint32_t[max_subsize * 2];
  segment_rev = new bool[max_subsize * 2];
  segment_next = new uint32_t[max_subsize * 2];
  segment_prev = new uint32_t[max_subsize * 2];

  Solution sol(vs, max_id);
  init(sol);
}

void TwoLevelTree::validate() const
{
  uint32_t initial_id = 0;
  while(city_parent[initial_id] == -1)
  {
	initial_id++;
  }

  const uint32_t end_id = initial_id;
  int counter = 0;
  do
  {
	assert(city_parent[initial_id] != 4294967295);
	assert(prev(next(initial_id)) == initial_id);
	assert(next(prev(initial_id)) == initial_id);
	
	initial_id = next(initial_id);
	counter++;
  } while(initial_id != end_id);
  assert(_size == counter);
}

TwoLevelTree::TwoLevelTree(const Solution &sol)
  : max_id(sol.get_max_id())
{
  city_next = new uint32_t[sol.get_max_id()];
  city_prev = new uint32_t[sol.get_max_id()];
  city_parent = new uint32_t[sol.get_max_id()];

  const uint32_t max_subsize = ceil(sqrt(sol.get_max_id()));

  segment_back = new uint32_t[max_subsize * 2];
  segment_front = new uint32_t[max_subsize * 2];
  segment_size = new uint32_t[max_subsize * 2];
  segment_rev = new bool[max_subsize * 2];
  segment_next = new uint32_t[max_subsize * 2];
  segment_prev = new uint32_t[max_subsize * 2];

  init(sol);
}

bool TwoLevelTree::contain(const uint32_t city_id) const
{
  return city_parent[city_id] != -1;
}

// new_cityidが完全に新しい都市番号である前提
void TwoLevelTree::insert(const uint32_t new_cityid, const uint32_t orig_cityid)
{
  assert(!contain(new_cityid));
  assert(contain(orig_cityid));
  
  const uint32_t new_segmentid = city_parent[orig_cityid];
  const uint32_t city_next = next(orig_cityid);
  city_parent[new_cityid] = new_segmentid;
  
  next(new_cityid) = city_next;
  prev(new_cityid) = orig_cityid;
  prev(city_next) = new_cityid;
  next(orig_cityid) = new_cityid;

 if(back(new_segmentid) == orig_cityid)
 {
   back(new_segmentid) = new_cityid;
 }
 segment_size[new_segmentid]++;
 _size++;
}

void TwoLevelTree::init(const Solution &sol)
{
  // contain判定に使用する
  for(int i = 0; i < max_id; i++)
  {
	city_parent[i] = -1;
  }
  
  _size = sol.size();
  subsize = ceil(sqrt(sol.size()));
  
  std::vector<size_t> seg_accum_size(subsize);
  for(int i = 0; i < subsize; i++)
  {
	seg_accum_size[i] = size() * (i + 1) / subsize;
  }
  
  segment_size[0] = seg_accum_size[0];
  for(int i = 1; i < subsize; i++)
  {
	segment_size[i] = seg_accum_size[i] - seg_accum_size[i - 1];
  }
  
  for(int i = 0; i < subsize - 1; i++)
  {
	segment_next[i] = i + 1;
	segment_prev[i + 1] = i;
  }
  segment_prev[0] = subsize - 1;
  segment_next[subsize - 1] = 0;
  
  uint32_t seg_id = 0;
  uint32_t id = 0;
  segment_front[seg_id] = id;
  for(uint32_t i = 0; i < size(); i++, id = city_next[id])
  {
	if(i == seg_accum_size[seg_id])
	{
	  seg_id++;
	  // segment_front
	  segment_front[seg_id] = id;
	}
	
	city_next[id] = sol.next(id);
	city_prev[id] = sol.prev(id);
	city_parent[id] = seg_id;
	
	// segment_back
	if(i + 1 == seg_accum_size[seg_id])
	{
	  segment_back[seg_id] = id;
	}
  }

  for(uint32_t seg_index = 0; seg_index < subsize; seg_index++)
  {
	segment_rev[seg_index] = false;
  }
}


TwoLevelTree::~TwoLevelTree()
{
  delete[] city_prev;
  delete[] city_next;
  delete[] city_parent;
  
  delete[] segment_back;
  delete[] segment_front;
  delete[] segment_size;
  delete[] segment_rev;
  delete[] segment_next;
  delete[] segment_prev;
}


size_t TwoLevelTree::size() const
{
  return _size;
}

uint32_t &TwoLevelTree::next(const uint32_t id) const
{
  return segment_rev[city_parent[id]] ? city_prev[id] : city_next[id];
}


uint32_t &TwoLevelTree::prev(const uint32_t id) const
{
  return segment_rev[city_parent[id]] ? city_next[id] : city_prev[id];
}


uint32_t &TwoLevelTree::front(const uint32_t segid)
{
  return segment_rev[segid] ? segment_back[segid] : segment_front[segid];
}


uint32_t &TwoLevelTree::back(const uint32_t segid)
{
  return segment_rev[segid] ? segment_front[segid] : segment_back[segid];
}


void TwoLevelTree::flip_insegment(const uint32_t from, const uint32_t to)
{
  const uint seg_from = city_parent[from];
  const uint seg_to = city_parent[to];
  
  if(from == front(seg_from) && to == back(seg_to))
  {
	rev_seg(seg_from, seg_to);
  }
  else if(from == front(seg_from))
  {
	const uint32_t to_end_seg = subsize;
	split(next(to), to_end_seg);
	rev_seg(seg_from, seg_from);
	merge(to_end_seg);
  }
  else if(to == back(seg_from))
  {
	const uint32_t from_to_seg = subsize;
	split(from, from_to_seg);
	rev_seg(from_to_seg, from_to_seg);
	merge(from_to_seg);
  }
  else
  {
	const uint32_t from_to_seg = subsize;
	split(from, from_to_seg);
	const uint32_t to_end_seg = subsize + 1;
	split(next(to), to_end_seg);
	
	rev_seg(from_to_seg, from_to_seg);
	merge(to_end_seg);	  
	merge(from_to_seg);
  }
}

void TwoLevelTree::flip_outsegment(const uint32_t from, const uint32_t to)
{
  const uint seg_from = city_parent[from];
  const uint seg_to = city_parent[to];
  
  if(from == front(seg_from) && to == back(seg_to))
  {
	rev_seg(seg_from, seg_to);
  }
  else if(from == front(seg_from))
  {
	const uint32_t to_end_seg = subsize;
	split(next(to), to_end_seg);
	rev_seg(seg_from, seg_to);
	merge(to_end_seg);
  }
  else if(to == back(seg_to))
  {
	const uint32_t from_end_seg = subsize;
	split(from, from_end_seg);
	rev_seg(from_end_seg, seg_to);
	merge(from_end_seg);
  }
  else
  {
	const uint32_t from_end_seg = subsize;
	split(from, from_end_seg);
	
	const uint32_t to_end_seg = subsize + 1;
	split(next(to), to_end_seg);
	
	rev_seg(from_end_seg, seg_to);
	
	merge(to_end_seg);
	merge(segment_prev[from_end_seg]);
  }
}

// [from, to] 

void TwoLevelTree::flip(const uint32_t from, const uint32_t to)
{
  if(segment_size[city_parent[from]] == 1 and segment_size[city_parent[to]] == 1)
  {
  	rebuild();
  }
  
  const uint seg_from = city_parent[from];
  const uint seg_to = city_parent[to];

  if(seg_from != seg_to)
  {
	flip_outsegment(from, to);
  }
  else if(betweenInSegment(from, to, back(seg_from)))
  {
	flip_insegment(from, to);
  }
  else
  {
	flip_outsegment(from, to);
  }  
}

void TwoLevelTree::rebuild()
{
  std::vector<uint32_t> vs(size());
  uint32_t id = 0;
  for(int i = 0; i < size(); i++)
  {
	vs[i] = id;
	id = next(id);
  }
  Solution sol(vs, max_id);
  init(sol);
}

void TwoLevelTree::rev_seg(const uint32_t seg_from, const uint32_t seg_to)
{
  const uint32_t prev_from = segment_prev[seg_from];
  const uint32_t next_to = segment_next[seg_to];

  const uint32_t end_seg = segment_next[seg_to];
  uint32_t seg_id = seg_from;

  if(seg_id == end_seg)
  {
	// seg_id == end_segなら全部swapしきるので，余計なprev/nextの付け替えは必要なし
	do
	{
	  std::swap(segment_next[seg_id], segment_prev[seg_id]);
	  segment_rev[seg_id] = !segment_rev[seg_id];
	  seg_id = segment_prev[seg_id];
	} while(seg_id != end_seg);
  }
  else
  {
	while(seg_id != end_seg)
	{
	  std::swap(segment_next[seg_id], segment_prev[seg_id]);
	  segment_rev[seg_id] = !segment_rev[seg_id];
	  seg_id = segment_prev[seg_id];
	}
	
	// seg_from == seg_toでもOK
	segment_next[prev_from] = seg_to;
	segment_prev[next_to] = seg_from;
	
	segment_next[seg_from] = next_to;
	segment_prev[seg_to] = prev_from;
	
	next(back(prev_from)) = front(seg_to);
	next(back(seg_from)) = front(next_to);
	
	prev(front(next_to)) = back(seg_from);
	prev(front(seg_to)) = back(prev_from);
  }
}

// 全探索between

bool TwoLevelTree::betweenInSegment(const uint32_t a, const uint32_t b, const uint32_t c) const
{
  const uint32_t end_id = next(c);
  uint32_t city_id = a;
  while(city_id != end_id)
  {
	if(city_id == b) return true;
	city_id = next(city_id);
  }
  return false;
}

// 注意: flipに限定すれば，新規idはsubsize, subsize + 1で十分
// 

void TwoLevelTree::split(const uint32_t city_from, const uint32_t new_segid)
{
  const uint32_t seg_id = city_parent[city_from];
  
  // segment_rev
  segment_rev[new_segid] = segment_rev[seg_id];

  const uint32_t end_id = next(back(seg_id));
  
  // city_parent
  uint32_t city_id = city_from;
  uint32_t new_segment_size = 0;
  while(city_id != end_id)
  {
	city_parent[city_id] = new_segid;
	new_segment_size++;
	city_id = next(city_id);
  }
  
  front(new_segid) = city_from;
  back(new_segid) = back(seg_id);

  back(seg_id) = prev(city_from);

  const uint32_t next_new = segment_next[seg_id];

  // 元々seg_idの次だったやつのprevをnew_segidにする必要あり
  segment_size[new_segid] = new_segment_size;
  segment_size[seg_id] -= new_segment_size;
  
  segment_prev[next_new] = new_segid;
  segment_next[seg_id] = new_segid;
  
  segment_next[new_segid] = next_new;
  segment_prev[new_segid] = seg_id;
}


void TwoLevelTree::merge(const uint32_t seg_id1)
{
  const uint32_t seg_id2 = segment_next[seg_id1];
  
  assert(segment_next[seg_id1] == seg_id2);
  
  if(segment_rev[seg_id1] != segment_rev[seg_id2])
  {
	// if(segment_rev[seg_id1])
	if(segment_size[seg_id1] < segment_size[seg_id2])
	{
	  revert_rev(seg_id1);
	}
	else
	{
	  revert_rev(seg_id2);
	}
  }
  assert(segment_rev[seg_id1] == segment_rev[seg_id2]);

  if(seg_id1 > seg_id2)
  {
	const uint32_t end_id = next(back(seg_id1));
	uint32_t city_id = front(seg_id1);
	while(city_id != end_id)
	{
	  city_parent[city_id] = seg_id2;
	  city_id = next(city_id);
	}
	segment_next[segment_prev[seg_id1]] = seg_id2;
	segment_size[seg_id2] += segment_size[seg_id1];

	segment_prev[seg_id2] = segment_prev[seg_id1];
	front(seg_id2) = front(seg_id1);
  }
  else
  {
	const uint32_t end_id = next(back(seg_id2));
	uint32_t city_id = front(seg_id2);
	while(city_id != end_id)
	{
	  city_parent[city_id] = seg_id1;
	  city_id = next(city_id);
	}

	segment_prev[segment_next[seg_id2]] = seg_id1;
	segment_size[seg_id1] += segment_size[seg_id2];

	segment_next[seg_id1] = segment_next[seg_id2];
	back(seg_id1) = back(seg_id2);
  }
}


void TwoLevelTree::revert_rev(const uint32_t segid)
{
  const uint32_t end_id = next(back(segid));
  uint32_t city_id = front(segid);
  while(city_id != end_id)
  {
	std::swap(next(city_id), prev(city_id));
	city_id = prev(city_id); // swapする前のnext
  }
  segment_rev[segid] = !segment_rev[segid];
  std::swap(segment_front[segid], segment_back[segid]);
}


std::ostream &operator<<(std::ostream &out, const TwoLevelTree &tree)
{
  // 0を最初に記述する縛り
  uint32_t id = 0;
  for(int i = 0; i < tree.size(); i++)
  {
	out << id << " ";
	id = tree.next(id);
  }
  return out;
}
