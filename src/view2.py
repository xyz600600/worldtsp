from pylab import *

size = 40000

xs = []
ys = []

home = '/ramdisk/tour/'

for line in open('/home/xyz600/data/world.tsp').readlines()[:size]:
    tokens = line.strip().split(" ")
    ys.append(float(tokens[1]))
    xs.append(float(tokens[2]))

index = 238

lst_test = [675, 676, 183, 185, 197, 724, 184, 186, 478, 677, 689, 681, 614, 193, 200, 219, 213, 636, 647, 635, 640, 632, 641, 214, 196, 302, 198, 248, 492, 203, 208, 514, 515, 502, 465, 210, 205, 209, 368, 282, 367, 211, 216, 267, 281, 280, 222, 234, 243, 232]

# lst_ans = [193, 200, 225, 219, 213, 636, 647, 635, 640, 632, 641, 227, 214, 228, 196, 230, 302, 231, 198, 248, 492, 203, 303, 208, 514, 515, 254, 247, 249, 261, 252, 502, 253, 465, 210, 205, 209, 368, 239, 282, 367, 211, 216, 267, 281, 280, 222, 234, 243, 232]

lst_ans = [225, 227, 228, 230, 231, 303, 254, 247, 249, 261, 252, 253, 239]

plt_xs = []
plt_ys = []

for i in lst_test:
    plt_xs.append(xs[i])
    plt_ys.append(ys[i])

ans_xs = []
ans_ys = []

for i in lst_ans:
    ans_xs.append(xs[i])
    ans_ys.append(ys[i])
    
clf()

plot(xs, ys, 'ko')
plot(xs[index], ys[index], 'ro')
plot(plt_xs, plt_ys, 'bo')
plot(ans_xs, ans_ys, 'yo')

show()    
