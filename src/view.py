from pylab import *

size = 1

xs = []
ys = []

home = '/ramdisk/tour/'

for line in open('/home/xyz600/data/world.tsp').readlines():
    tokens = line.strip().split(" ")
    ys.append(float(tokens[1]))
    xs.append(float(tokens[2]))

for i in range(1, size + 1):
    nodes = list(map(int, open(home + 'data/tour.txt').readlines()[0].strip().split(" ")))

    plt_xs = []
    plt_ys = []
    
    for j in nodes:
        plt_xs.append(xs[j])
        plt_ys.append(ys[j])
        
    plt_xs.append(xs[nodes[0]])
    plt_ys.append(ys[nodes[0]])

    clf()
    xlim([-1800000, 1800000])
    ylim([-800000, 800000])

    plot(xs[0], ys[0], 'bo')
    plot(plt_xs, plt_ys, 'b-', linewidth=0.15)
    savefig(home + 'png_img/fig.png', dpi=500)
    
    print(i)
