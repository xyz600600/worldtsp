#pragma once
#include <array>
#include <cstdint>
#include <cmath>
#include <fstream>
#include <iostream>

class DataHolder
{
public:
  DataHolder(const std::string &path, const uint32_t size);
  ~DataHolder();
  double latitude(uint32_t index) const;
  double longtitude(uint32_t index) const;
  uint64_t distance_of(uint32_t i, uint32_t j) const;
  uint64_t distance_of(double lat1, double long1, double lat2, double long2) const;
  
  // dists = [d(i, js[0]), d(i, js[1]), d(i, js[2]), d(i, js[3])]
  void distance_of_4(uint32_t i, uint32_t *js, uint64_t *dists) const;

  // dists = [d(is[0], js[0]), d(is[1], js[1]), d(is[2], js[2]), d(is[3], js[3])]
  void distance_of_4(uint32_t *is, uint32_t *js, uint64_t *dists) const;

  void distance_of_4(const double lat1, const double long1,
					 const double *const lat2s, const double *const long2s,
					 uint64_t *const dists) const;

  uint32_t size() const;
private:
  double *lats;
  double *longs;
  const uint32_t _size;

  // __m256i distance_of_4(__m256d &lat1s, __m256d &long1s,
  // 						__m256d &lat2s, __m256d &long2s);
};

uint32_t DataHolder::size() const
{
  return _size;
}

double DataHolder::latitude(uint32_t index) const
{
  assert(0 <= index && index < size());
  return lats[index];
}

double DataHolder::longtitude(uint32_t index) const 
{
  assert(0 <= index && index < size());
  return longs[index];
}

#undef M_PI
#define M_PI 3.14159265358979323846264

DataHolder::DataHolder(const std::string &path, const uint32_t size)
  : _size(size)
{
  lats = new double[size]; //reinterpret_cast<double *>(_mm_malloc(sizeof(double) * size, 32));
  longs = new double[size]; //reinterpret_cast<double *>(_mm_malloc(sizeof(double) * size, 32));
	
  std::ifstream fin(path, std::ios::in);
  if(fin.fail())
  {
	std::cerr << "fail to open " << path << std::endl;
	return;
  }
			  
  for(int i = 0; i < size; i++)
  {
	int32_t index;
	double lat, log;
	fin >> index >> lat >> log;
	lats[i] = M_PI * lat / 1800000.0;
	longs[i] = M_PI * log / 1800000.0;
  }
}

DataHolder::~DataHolder()
{
  delete[] lats;
  delete[] longs;
}

// 公式のdistance_of
// uint64_t geom_edgelen (int i, int j, vector<Pos> &positions)
// {
//   double lati = M_PI * latitude(positions[i]) / 1800000.0;
//   double latj = M_PI * latitude(positions[j]) / 1800000.0;
  
//   double longij_d = M_PI * (longtitude(positions[i]) - longtitude(positions[j])) / 1800000.0;
  
//   double q1 = cos (latj) * sin(longij_d);
//   double q3 = sin(longij_d/2.0);
//   double q4 = cos(longij_d/2.0);
//   double q2 = sin(lati + latj) * q3 * q3 - sin(lati - latj) * q4 * q4;
//   double q5 = cos(lati - latj) * q4 * q4 - cos(lati + latj) * q3 * q3;
//   return static_cast<uint64_t>(6378388.0 * atan2(sqrt(q1*q1 + q2*q2), q5) + 1.0);
// }

double square(double v)
{
  return v * v;
}

// 自身と距離計算は禁止
uint64_t DataHolder::distance_of(double lat1, double long1, double lat2, double long2) const
{
  // return static_cast<uint64_t>(6378388.0 * sqrt(square(lat1 - lat2) + square(long1 - long2)));
  
  double latipj = lat1 + lat2;
  double latimj = lat1 - lat2;
  double longij_d = long1 - long2;

  double cos_latipj = cos(latipj);
  double cos_latimj = cos(latimj);
  
  double cos_w = (-cos_latipj + cos_latimj + cos(longij_d) * (cos_latimj + cos_latipj)) / 2;
  return static_cast<uint64_t>(6378388.0 * acos(cos_w)) + 1;
}

// __m256i DataHolder::distance_of_4(__m256d &lat1s, __m256d &long1s,
// 										__m256d &lat2s, __m256d &long2s)
// {
  // cosineがSIMDにない!!!
  // __m256d latipj = _mm256_add_pd(lat1s, lat2s);
  // __m256d latimj = _mm256_sub_pd(lat1s, lat2s);
  
  // __m256d cos_latipj = _mm256_cos_pd<double>(latipj);
  // __m256d cos_latimj = _mm256_cos_pd(latimj);

  // __m256d longij_d = _mm256_sub_pd(long1s, long2s);
  // __m256d cos_longij_d = _mm256_cos_pd(longij_d);

  // __m256d cpc = _m256_add_pd(cos_latipj, cos_latimj);
  // __m256d cpcmcpc = _mm256_fmadd_pd(cpc, cos_longij_d, cos_latimj);
  // __m256d cos_w_m2 = _mm256_sub_pd(cpcmcpc, cos_latipj);
  // __m256d half = _mm256_set1_pd(0.5);
  // __m256d cos_w = _mm256_mul_pd(cos_w_m2, half);

  // __m256d acos_w = _mm256_acos_ps(cos_w);
  // __m256d radius = _mm256_set1_pd(6378388.0);
  // __m256d ansmhalf = _mm256_fmadd_pd(acos_w, radius, half);
  // __m256d double_ans = _mm256_add_pd(ansmhalf, half);
  // __m128i int_ans = _mm256_cvttpd_epi32(double_ans);

  // return _mm256_cvtepu32_epi64(int_ans);
// }

void DataHolder::distance_of_4(const double lat1, const double long1,
									 const double *const lat2s, const double *const long2s,
									 uint64_t *const dists) const
{
  dists[0] = distance_of(lat1, long1, lat2s[0], long2s[0]);
  dists[1] = distance_of(lat1, long1, lat2s[1], long2s[1]);
  dists[2] = distance_of(lat1, long1, lat2s[2], long2s[2]);
  dists[3] = distance_of(lat1, long1, lat2s[3], long2s[3]);
}

void DataHolder::distance_of_4(uint32_t i, uint32_t *js, uint64_t *dists) const
{
  dists[0] = distance_of(i, js[0]);
  dists[1] = distance_of(i, js[1]);
  dists[2] = distance_of(i, js[2]);
  dists[3] = distance_of(i, js[3]);
}

void DataHolder::distance_of_4(uint32_t *is, uint32_t *js, uint64_t *dists) const
{
  dists[0] = distance_of(is[0], js[0]);
  dists[1] = distance_of(is[1], js[1]);
  dists[2] = distance_of(is[2], js[2]);
  dists[3] = distance_of(is[3], js[3]);
}

uint64_t DataHolder::distance_of(uint32_t i, uint32_t j) const
{
  return distance_of(latitude(i), longtitude(i), latitude(j), longtitude(j));
}
