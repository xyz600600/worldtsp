#pragma once

#include <vector>
#include <cstdint>
#include <queue>
#include <iostream>

#include "KDTree.h"
#include "UnionFind.h"

class Kruskal
{
public:
  void getMST(std::vector<std::vector<uint32_t>> &graph);
  Kruskal(const KDTree &tree);
  
private:
  const KDTree &tree;
  
};

Kruskal::Kruskal(const KDTree &tree)
  : tree(tree){}

void Kruskal::getMST(std::vector<std::vector<uint32_t>> &graph)
{
  assert(graph.size() == tree.size());
  
  std::vector<std::vector<std::pair<uint64_t, uint32_t>>> neighbor(tree.size());
  std::vector<uint32_t> accum_index(tree.size()); // index + accum_index[index] = kがk-th nearest
  std::fill(accum_index.begin(), accum_index.end(), 0);

  const uint32_t subsize = tree.size() / 10;
  std::cerr << "subsize = " << subsize << std::endl;
  
  for(uint32_t i = 0; i < tree.size(); i++)
  {
	if(i % 100000 == 0) std::cerr << i << std::endl;
	const uint32_t add_size = tree.size() / subsize;
	std::priority_queue<std::pair<uint64_t, uint32_t>> que;
	// RadixHeap que;
	tree.nearest_search(i, add_size, que);

	neighbor[i].resize(add_size);
	for(int j = add_size - 1; j >= 0; j--)
	{
	  neighbor[i][j] = que.top();
	  que.pop();
	}
  }

  using type_pq = std::pair<uint64_t, std::pair<uint32_t, uint32_t>>;
  std::priority_queue<type_pq, std::vector<type_pq>, std::greater<type_pq>> que;
  
  for(uint32_t i = 0; i < tree.size(); i++)
  {
	que.push(std::make_pair(neighbor[i][0].first, std::make_pair(i, 0UL)));
  }

  UnionFind uf(tree.size());

  int counter = 0;
  
  int edge_counter = 0;
  while(!que.empty() and edge_counter < tree.size() - 1)
  {
	auto elem = que.top();
	que.pop();
	uint64_t dist = elem.first;
	uint32_t id, index;
	std::tie(id, index) = elem.second;

	const uint32_t id2 = neighbor[id][index].second;

	if(++counter % 1000000 == 0)
	{
	  std::cerr << counter << std::endl;
	  std::cerr << "edge counter: " << edge_counter << std::endl;
	  std::cerr << "id1, id2 = " << id << ", " << id2 << ", " << std::endl;
	  std::cerr << "distance = " << neighbor[id][index].first << std::endl;
	}

	assert(0 <= id and id < tree.size() and
		   0 <= id2 and id2 < tree.size());
	
	if(uf.find(id) != uf.find(id2))
	{
	  uf.uni(id, id2);
	  edge_counter++;

	  graph[id].push_back(id2);
	  graph[id2].push_back(id);
	}

	index++;
	uint32_t diff = (accum_index[id] + 1) * tree.size() / subsize - accum_index[id] * tree.size() / subsize;
	if(index >= diff)
	{
	  accum_index[id]++;
	  index = 0;
	  if(accum_index[id] < subsize)
	  {
		const uint32_t diff_next = (accum_index[id] + 1) * tree.size() / subsize - accum_index[id] * tree.size() / subsize;
		// std::cerr << accum_index[id] << ", " << diff_next << std::endl;
		// 近傍リストをsqrt(N)個ずつ更新
		std::priority_queue<std::pair<uint64_t, uint32_t>> q;
		// RadixHeap q;
		tree.nearest_search_lower(id, diff_next, q, id2, dist);
		
		neighbor[id].resize(diff_next);
		for(int j = diff_next - 1; j >= 0; j--)
		{
		  neighbor[id][j] = q.top();
		  q.pop();
		}
	  }
	}
	if(index + accum_index[id] * tree.size() / subsize < tree.size())
	{
	  que.push(std::make_pair(neighbor[id][index].first, std::make_pair(id, index)));
	}
  }
  std::cerr << "edge counter: " << edge_counter << std::endl;
}
